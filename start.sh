#!/bin/bash

set -eu -o pipefail

cd /app/code

echo "=========================="
echo "     Radicale startup     "
echo "=========================="

echo "=> Update radicale config"
sed -e "s/ldap_uri = ldap:\/\/.*/ldap_uri = ldap:\/\/${CLOUDRON_LDAP_SERVER}:${CLOUDRON_LDAP_PORT}/" \
    -e "s/ldap_base = .*/ldap_base = ${CLOUDRON_LDAP_USERS_BASE_DN}/" \
    -e "s/ldap_reader_dn = .*/ldap_reader_dn = ${CLOUDRON_LDAP_BIND_DN}/" \
    -e "s/ldap_secret = .*/ldap_secret = ${CLOUDRON_LDAP_BIND_PASSWORD}/" \
    "/app/pkg/config" > "/run/config"

if [[ ! -f /app/data/rights ]]; then
    echo "=> Copy default /app/data/rights file"
    cp /app/pkg/rights.template /app/data/rights
fi

echo "=> Ensure folder permissions"
chown -R cloudron:cloudron /app/data

echo "=> Start radicale"
exec /usr/local/bin/gosu cloudron:cloudron radicale --config /run/config
