FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# renovate: datasource=pypi depName=radicale versioning=pep440
ARG RADICALE_VERSION=3.4.1

RUN pip install ldap3 radicale==${RADICALE_VERSION}

ADD config start.sh rights.template /app/pkg/

CMD [ "/app/pkg/start.sh" ]
