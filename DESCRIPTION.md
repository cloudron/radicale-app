### About

The Radicale Project is a CalDAV (calendar) and CardDAV (contact) server solution.

Calendars and address books can be viewed, edited and synced by calendar and contact clients on mobile phones or computers.

Officially supported clients are listed [here](https://radicale.org/v3.html#supported-clients), other DAV compatible clients may work as well.

