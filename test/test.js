#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

require('chromedriver');

const execSync = require('child_process').execSync,
    ejs = require('ejs'),
    expect = require('expect.js'),
    fs = require('fs'),
    url = require('url'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

const VDIRSYNCER = process.env.VDIRSYNCER || 'vdirsyncer';

console.log('===============================================');
console.log(' This test requires vdirsyncer to be installed (using ' + VDIRSYNCER + ' )');
console.log('===============================================');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const TEST_TIMEOUT = 10000;

    let collectionId;
    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;
    const collectionName = 'mycollection';

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function login() {
        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn + '/.web/');
        await waitForElement(By.xpath('//input[@data-name="user"]'));
        await browser.findElement(By.xpath('//input[@data-name="user"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@data-name="password"]')).sendKeys(password);
        await browser.findElement(By.tagName('form')).submit();
        await waitForElement(By.xpath('//a[contains(., "Logout")]'));
    }

    async function logout() {
        await browser.get('https://' + app.fqdn + '/.web/');
        await waitForElement(By.xpath('//input[@data-name="user"]'));
        await browser.findElement(By.xpath('//input[@data-name="user"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@data-name="password"]')).sendKeys(password);
        await browser.findElement(By.tagName('form')).submit();
        await waitForElement(By.xpath('//a[.="Logout"]'));
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//a[.="Logout"]')).click();
        await waitForElement(By.xpath('//input[@data-name="user"]'));
    }

    async function createCollection() {
        await browser.get('https://' + app.fqdn + '/.web/');
        await waitForElement(By.xpath('//input[@data-name="user"]'));
        await browser.findElement(By.xpath('//input[@data-name="user"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@data-name="password"]')).sendKeys(password);
        await browser.findElement(By.tagName('form')).submit();
        await waitForElement(By.xpath('//a[@data-name="new"]'));
        await browser.findElement(By.xpath('//a[@data-name="new"]')).click();
        await waitForElement(By.xpath('//section[@id="createcollectionscene"]/*/input[@data-name="displayname"]'));
        await browser.findElement(By.xpath('//section[@id="createcollectionscene"]/*/input[@data-name="displayname"]')).sendKeys(collectionName);
        await browser.findElement(By.xpath('//section[@id="createcollectionscene"]/*/select[@data-name="type"]/option[1]')).click();   // select addressbook
        await browser.findElement(By.xpath('//section[@id="createcollectionscene"]/*/button[@type="submit"]')).click();
        await waitForElement(By.xpath('//h3[text()="' + collectionName + '"]'));
    }

    async function getCollection() {
        browser.get('https://' + app.fqdn + '/.web/');
        await browser.sleep(5000);
        await waitForElement(By.xpath('//input[@data-name="user"]'));
        await browser.findElement(By.xpath('//input[@data-name="user"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@data-name="password"]')).sendKeys(password);
        await browser.findElement(By.tagName('form')).submit();
        await waitForElement(By.xpath('//a[@data-name="new"]'));

        const href = await browser.findElement(By.xpath('//section[@id="collectionsscene"]/article[1]//a[1]')).getAttribute('href');
        var tmp = url.parse(href);
        collectionId = tmp.path.split('/').filter(function (f) { return f; }).pop();

        console.log('Using collection: ', collectionId);
    }

    function syncAddressBook() {
        fs.rmSync('/tmp/vdirsyncer', { force: true, recursive: true });

        const statusPath = '/tmp/vdirsyncer/status/';
        const addressBookPath = '/tmp/vdirsyncer/addressbook/';
        const testContactFile = __dirname + '/test_contact.vcf';

        fs.mkdirSync(statusPath, { recursive: true });
        fs.mkdirSync(addressBookPath, { recursive: true });

        const configFile = '/tmp/vdirsyncer/config';
        const data = {
            statusPath: statusPath,
            addressBookPath: addressBookPath,
            cardDavUrl: 'https://' + app.fqdn + '/' + username + '/' + collectionId + '/',
            username: username,
            password: password
        };

        fs.writeFileSync(configFile, ejs.render(fs.readFileSync('vdirsyncer.conf.ejs', 'utf-8'), data));
        fs.writeFileSync(addressBookPath + 'test_contact.vcf', fs.readFileSync(testContactFile));

        const env = process.env;
        env.VDIRSYNCER_CONFIG = configFile;
        execSync(VDIRSYNCER + ' discover ', { env: env, stdio: 'inherit' });
        execSync(VDIRSYNCER + ' sync', { env: env, stdio: 'inherit' });

        expect(fs.readFileSync(testContactFile, 'utf-8')).to.equal(fs.readFileSync(addressBookPath + 'test_contact.vcf', 'utf-8'));
    }

    function checkAddressBook() {
        fs.rmSync('/tmp/vdirsyncer', { force: true, recursive: true });

        const statusPath = '/tmp/vdirsyncer/status/';
        const addressBookPath = '/tmp/vdirsyncer/addressbook/';
        const testContactFile = __dirname + '/test_contact.vcf';

        fs.mkdirSync(statusPath, { recursive: true });
        fs.mkdirSync(addressBookPath, { recursive: true });

        const configFile = '/tmp/vdirsyncer/config';
        const data = {
            statusPath: statusPath,
            addressBookPath: addressBookPath,
            cardDavUrl: 'https://' + app.fqdn + '/' + username + '/' + collectionId + '/',
            username: username,
            password: password
        };

        fs.writeFileSync(configFile, ejs.render(fs.readFileSync('vdirsyncer.conf.ejs', 'utf-8'), data));

        const env = process.env;
        env.VDIRSYNCER_CONFIG = configFile;
        console.log('VDIRSYNCER_CONFIG=/tmp/vdirsyncer/config vdirsyncer sync');
        execSync(VDIRSYNCER + ' discover test_contacts', { env: env, stdio: 'inherit' });
        execSync(VDIRSYNCER + ' sync', { env: env, stdio: 'inherit' });

        const contactRemoteFileName = fs.readdirSync(addressBookPath)[0];
        expect(contactRemoteFileName).to.be.a('string');

        const testContactFileContent = fs.readFileSync(testContactFile, 'utf-8');
        let testContactRemoteFileContent = fs.readFileSync(addressBookPath + '/' + contactRemoteFileName, 'utf-8');

        // remove the dynamic radicale id
        testContactRemoteFileContent = testContactRemoteFileContent.replace('X-RADICALE-NAME:' + contactRemoteFileName + '\n', '');

        expect(testContactFileContent).to.equal(testContactRemoteFileContent);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('can create collection', createCollection);
    it('can get collection id', getCollection);
    it('can logout', logout);

    it('can sync addressbook', syncAddressBook);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', login);
    it('can get collection id', getCollection);
    it('can logout', logout);
    it('addressbook is still ok', checkAddressBook);

    it('can restart app', function () { execSync('cloudron restart --app ' + app.id); });

    it('can login', login);
    it('can get collection id', getCollection);
    it('can logout', logout);
    it('addressbook is still ok', checkAddressBook);

    it('move to different location', async function () {
        await browser.manage().deleteAllCookies();

        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron configure --app ${app.id} --location ${LOCATION}2`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);

    it('can login', login);
    it('can get collection id', getCollection);
    it('can logout', logout);
    it('addressbook is still ok', checkAddressBook);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app for update', function () { execSync(`cloudron install --appstore-id org.radicale.cloudronapp2 --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can get app information', getAppInfo);
    it('can login', login);
    it('can create collection', createCollection);
    it('can get collection id', getCollection);
    it('can logout', logout);

    it('can sync addressbook', syncAddressBook);

    it('can update', function () { execSync(`cloudron update --app ${LOCATION}`, EXEC_ARGS); });

    it('can login', login);
    it('can get collection id', getCollection);
    it('can logout', logout);

    it('addressbook is still ok', checkAddressBook);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
